
# Structure

## Path look up

If the path argument starts with "*/", it's a path relative to the start of the cartridge path.

If the path argument starts with "~/", it's a path relative to the current cartridge in the cartridge path.

A path argument that doesn't start with "./" or "../" is resolved as top-level module.

A path argument that prepends "dw", or that starts with "dw/", references B2C Commerce built-in functions and classes, for example:

```
var u = require( 'dw/util' ); // loads the classes in the util package, which can be then used as follows:

var h = new u.HashMap();
```

## The Modules Folder and the Server Module 

A server module in the SFRA global modules folder provides the routing functionality for controllers. Every SFRA controller requires this module. Any JavaScript module included in the modules folder is globally available and can be required without a path in any controller or script.

The following example requires the server module in the modules folder.

```
var server = require('server');
```

So this is referring to server.js in the modules/server folder

Modules directory is not a cartridge, so we don't have to include it on the cartridge path

## Images Storage

| IMAGE TYPE | EXAMPLES | LOCATION |
| - | - | - |
| Catalog | product, category, option, and recommendation images | Within the catalog's own static content directory: **(Version Directory)/units/(Your Catalog)/static/default/** |
| Non-catalog | custom object, slot (type HTML), promotion, and store images | Within the organization ("Sites") static content directory: **(Version Directory)/units/Sites/static/default/** |
| Content | content assets | located within its own site-specific content library directory:**(Version Directory)/units/Sites-(Your Site)-Library/static/default/** |


## Extension
Currently, B2C Commerce supports the following extensions, listed in priority order:
- .js - JavaScript file
- .ds - B2C Commerce script file
- .json - JSON file

# Logging

On peut définir le level minimum pour activer les logs au root et pour un log file. Then : 
```
var log = Logger.getLogger('disney');
log.warn('if there is one arg, this is the category and send to the main custom log file');
```
juste avec la categorie, ca créé un fichier par type de level, genre customwarn ou custominfo et ca rentre custom.disney : logtext

```
var log = Logger.getLogger('specificFile','disney');
log.warn('but with 2 args, the first one become the filename, and the second is the category');
//et la ca écrit tous les levels dans un fichier custom-specific file : custom.disney : tagada
```

Puis on peut aussi choisir à partir de quel niveau ca écrit sur le fichier ou pas

Ordre des niveaux de debug

Debug, info, warn, error, fatal

# Deploy
dw.json : 
- Hostname : sandbox address
- Username : account manager credentials
- Password
- Code-version : to target the correct deployed code


# Controller 

There is 2 ways to extends controller, it depends only if your controller file has the same name as the one you want to modify or not.

Same name: controller name like the original file, use superModule
```
server.extend(module.superModule);
server.replace(
    'Show',
	function (req, res, next) {
	[...]
```
Different name: controller named differently from the original file, in that case
```
server.extend(require(‘app_storefront_base/cartridge/controllers/Cart’));
[...]
```

La syntaxe peut aussi se faire ainsi :
```
var search = module.superModule;
var server = require('server');
server.extend(search); // ce qui revient à server.extend(module.superModule);
server.append('Show', function (req, res, next) {
[...]
```

The module.superModule global property provides access to the most recent module on the cartridge path module with the same path and name as the current module.

A controller cannot call another controller


In order to render template : deux solutions : 

```
Server.append( 'Show' , function (req, res, next) ) {
	res.setViewData ({
		data: myDataObject
	});
	res.render('/content/myPage');
	next();
});
```

<p style="text-align: center;">OU BIEN</p>

```
Server.append( 'Show' , function (req, res, next) ) {
	res.render('/content/myPage',{
			data: myDataObject
	});
	next();
});
```

---

Pour mieux voir le controller/function, on peut mettre les urls en mode 'friendly debug'
- `Merchant Tools > Site Preferences > Storefront URL` and disable Enable Storefront URLs

SFRA server module provides a few ways for us to extend a controller. You can add extra logic:
- Before the beginning of your function, using server.prepend
- At the end of the function, using server.append
- At the end of your function after the whole middleware chain (this is a special case), using `this.on(“route:BeforeComplete”)`
- Replace your whole function, using server.replace

---

```
module.exports = server.exports(); 
```
This line of code is always the last one in your controller and it makes all your endpoints visible to the external world so that they can be called

Pour faire cette ligne il faut faire le code (Well, we often put it at the beginning, so...)
```
require('server'); //refer to server.js in the modules/server folder (à la racine)
```

# Model
A model is the object that represent the data the controller sends to the view. It is a serializable JSON object

A decorator is a subset of the model that makes it easier to extend the model. decorators/index.js list all les decorators

To extend : 
1. use module.supermodule to identify the model to extend 
2. Use base.call() passing the same parameters that the base model needs

exemple pour étendre le decorateur availabity de base : `product/decorators/availability.js`
```
var base = module.superModule;
module.exports = function(object, a, b, etc) {
	base.call(this, object, a, b, c, etc); // invoke le decorator base model
	Object.defineProperty(object, 'ats', {
		enumerable : true, 
		value : getATS(c); // function à déclarer
	});
};
```


# Library and package import features
|Import packages   |  importPackages( dw.util );  |  Reuse processes that are outside the pipeline |
| ------------ | ------------ | ------------ |
| Import class  | importClass( dw.util.HashMap );  | Reuse classes that are outside the pipeline  |
| Library mechanism | importScript( "mylib:export/orderhandling.ds" );  | Reuse scripts that are outside the pipeline  |


# Custom object
Need to wrap into transaction to persist object : Transaction.wrap(function () {
```
CustomObjectMgr.getAllCustomObjects("tbdnewsletterid").asList().toArray();
var	tbdnewsletteridCustomObject : CustomObject = CustomObjectMgr.createCustomObject("tbdnewsletterid", UUIDUtils.createUUID());
```

# Custom cache
Dans package.json, ajouter une entrée pour définir le json de cache, et ensuite lui donner un ID dans le json approprié
```
var CacheMgr = require('dw/system/CacheMgr');
var cache = CacheMgr.getCache('UnlimitedCaching');

var toto = cache.get( 'tbdTestCacheEntry', function () {
	// Fonction qui calcule le truc a mettre en cache. Soit ca y ezt déjà, soit ca la recalcule
	return "!info en cache2!";
} );

logCache.warn('testcache {0}', toto);
// dw.system.Cache.invalidate(key) pour invalider
```


# Functions

## The Empty Operator
`empty()` Returns true if the object is empty.

`isEmpty()` Returns true if the collection is empty.

To get custom pref : 
```
var System = require('dw/system');
var site = System.Site.getCurrent();
var processingDays = site.getCustomPreferenceValue('myPreferenceID').getValue();
```
Et dans le template (assigner avant dans le controller) 
```
<p>info, processingDays configured in custom pref is ${pdict.processingDays}.</p>
```


# Templates 

ISML = Internet Store Markup Language // pdict = pipeline dictionnary

- `<isprint>` The `<isprint>` element ensures that special characters are HTML encoded (so always use it to display variables)
- `<isdecorate>` Decorate the enclosed content with the contents of the specified decorator template.
example : 
tpl1 : 
```
<isdecorate template=”decoratorFolder/pt_myDecorator”>
	…My content…to be decorated
</isdecorate>
```
tpl decoratorFolder/pt_myDecorator.isml  (par ex la page principale)
```
<html>
<head>…</head>
<body>
	This contains Header/Banner/Menus etc.
<isreplace/> <!--<<<<<<<<<<<<<<<ICI le decorator-->
This contains footer/Copyright notice etc.
</body>
<html>
```
- `<isinclude>` Enable content inclusion from an additional source. Either from another template or by executing a second pipeline that returns the information
- `<ismodule>` Build units of reusable template functionality that are inserted into the template using custom ISML tags
- - This is what they call Custom tags. It can be used like any other tags. In fact, a custom tag is an included template that can have user-defined attributes. Therefore, every custom tag definition is stored in a separate file. This method is similar to an include template.

example : 
```
<!--- tag declaration --->
<ismodule template = "TagExtension/background.isml" name = "bgcolor" attribute = "color">
```
```
<!--- tag usage --->
<isbgcolor color = "green">
```
Et background.isml peut utiliser ${color}

- `<iscomponent>` Include the output of a pipeline in the page.
- `<iscache>` For a page on your storefront to be cached, you must add the ISML tag `<iscache>` to the appropriate template. It is put as a </>, and work for the whole page, not only the enclosed content (that should not be enclosed)
```
<iscache type="relative" hour="2" minute="30"/>
```
The following line of code causes the resulting page to expire from the cache every day at 0630 hours (6:30 AM):
```
<iscache type="daily" hour="6" minute="30"/>
```
- `<iscomment>` Add documentation to the template.

Utility Functions in templates : 
Start with dw.system.N and N has one of the following value :
- Request : getHttpCookies(), getHttpHeaders(), etc
- Session : getCustomer(), isCustomerAuthenticated()
- StringUtils : formatDate(), stringToHtml(), trim(), etc
- URLUtils : url(), httpWebRoot(), etc


## translation : 
in template : 
```
${Resource.msg('address.field', 'my_resource_file', 'Rol Unico Tributario')}
```
The 2nd arg is the name of .properties file (without the extension), and the 3rd is the default text to show if the label couldn't be found.

- my_resource_file.properties: Default locale file 
- my_resource_file_es.properties: Localization file for all Spanish (ES) locales

Elles sont localiées dans template/ressources/nom_fr_FR.properties (et les templates dans templates/default/nom et templates/fr_FR/nom)

On peut même ajouter des templates spécifiques par locales : templates/default et templates/fr ou templates/fr_FR
Et le dossier resources pour les locales avec nom_default et nomdefault_fr et nomdefault_fr_FR

## Ajouter des assets 
```
<isscript>
	var assets = require('*/cartridge/script/assets.js');
	assets.addCss('/css/login.css');
	assets.addJs('/js/newsletter.js');
</isscript>
```

# Forms

The first thing you create for a form is the form definition. The form definition describes the data you need from the form, the data validation, and the system objects you want to store the data in. 
The form definition determines the structure of the in-memory form object. The in-memory form object persists data during the session, unless you explicitly clear the data.
Dans le dossier forms/default (ou la langue, fr, etc)

SFRAFormDef.xml
```
<?xml version="1.0"?>
<form xmlns="http://www.demandware.com/xml/form/2008-04-19">
	<field formid="nickname" label="Nickname:" type="string" mandatory="true" max-length="50" />
	<action formid="submit" valid-form="true"/>
	<action formid="cancel" valid-form="false"/>
</form>
```
SFRAForm.Js 
```
/**
 * A simple form controller.
 *
 */

'use strict';
var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get(
'Start', server.middleware.http, function (req, res, next) {
	var actionUrl = URLUtils.url('SFRAFormResult-Show'); //sets the route to call for the form submit action
	var SFRAhelloform = server.forms.getForm('SFRAFormDef'); //creates empty JSON object using the form definition
	 SFRAhelloform.clear();

   res.render('SFRAFormTemplate', { /* SFRAFormTemplate est le nom du template quî va être utilisé derrière */
       actionUrl: actionUrl,
       SFRAhelloform: SFRAhelloform
   });
 next();
});

module.exports = server.exports();
```
And template SFRAFormTemplate.isml
```
<div class="card">
	<form action="${pdict.actionUrl}" class="login" method="POST"
		name="SFRAHelloForm">

		<div class="form-group required">
			<label> Nickname: </label> <input type="input" id="nickname"
				class="form-control" name="nickname">
		</div>

		<button type="submit" class="btn btn-block btn-primary">Submit</button>
		<button type="submit" class="btn btn-block btn-primary">Cancel</button>
	</form>
</div>
```

# Content slot
There are three contexts of slots:
- Global slots can appear on any page.
- Category slots appear on category‐specific pages since they depend on the category ID.
- Folder Slots – appear in content library folders dependent on the folder ID.

Example : 
```
<isslot id="homepage_banner" description="Home banner 705px x 356px." context="global" />
```

## Content links
`$staticlink$` ‐ Creates a static link to an image.

`$url()$` ‐ Creates an absolute URL that retains the protocol of the outer request.

`$httpUrl()$` ‐ Creates an absolute URL, with the http protocol.

`$httpsUrl()$` ‐ Creates an absolute URL, with the https protocol.

`$include()$` ‐ Makes a remote include call (relevant for caching purposes).

Various sample : 

`$url('Product-Show','pid','<productId>')$`

`$url('Page-Show','cid','<content-asset-id>')$`

`$Url('Search-Show', 'cgid', 'nars-pro')$`

`$url('NarsPro-Show')$` -> Oui, this is a controller...


# Page designer

Code dans 
mycartridge/cartridge/experience/[component ou page]

Un fichier json pour décrire la page ou le composant et ce qu'il contient, et un script pour le js (ex meta definition file for a promotions page type is promopage.json, the script file is named promopage.js)

Pour les fonctions dans les scripts.js, c'est de ce format : 
```
module.exports.render = function (context) { 
```


# Job

A task-oriented script module is a CommonJS module that exposes a function to be called as the main function for the step with two parameters passed:
- Parameters configured by the business user are available as scriptable objects for the module’s function.
- stepExecution (dw.job.JobStepExecution): This object allows read-only access to information about the current step execution and job execution. To control the exit status.

A chunk-oriented script module reads and processes a list of items in chunks of size S and then writes a list of processed items. If the list contains more items than can be processed in one chunk, a new chunk is started, and up to S items are read, processed, and written.

Schedules only work in PIG instances so you must run manually in sandboxes.

A Job is declared in steptypes.json where we define the path to the script itself and the launch function. Job parameters can be used though pdict (like : `function launchTestFunction(pdict) {`)

In the end you export the function needed by the .json (can need more than one, in case of Chunk oriented  by example) : 
```
module.exports = {
	launchTestFunction: launchTestFunction
};
```



# OCAPI :: Open Commerce API

Sample json body : POST on https://{{env}}/dw/shop/v21_10/order_search?client_id=d32600b2-ad0d-4243-849b-7b40a2f510cc
```
	{
	  "start":0,
	  "count":200,  
	  "query": {
	    "filtered_query": {
	      "filter": {
	        "range_filter": {
	          "field": "creation_date",
	          "from": "2023-02-28T23:59:00.000Z",
	          "to": "2023-03-30T23:59:00.000Z"
	        }
	      },
	      "query": {
	                    "bool_query": {
	                        "must_not":[
	                            {"text_query":{"fields":["c_orderType"],"search_phrase":"fdsdfs"}}
	                        ],
	                        "must":[
	                            {"term_query":{"fields":["status"],"operator":"not_in","values":["failed","cancelled"]}},
	                            {"term_query":{"fields":["c_marketID"],"operator":"is","values":["FR"]}}
	                        ]
	                    }
	                }
	    }
	  },
	  "select": "(hits.(data.(site_id,order_total,c_marketID,currency)))",
	  "sorts" : [{"field":"creation_date", "sort_order":"asc"}]
	}
```

## Expansion
OCAPI expansion is a technique used in Salesforce Commerce Cloud's Open Commerce API to control the amount of data returned in an API response. By default, certain parts of a resource may be omitted to minimize network traffic and server CPU usage. However, if a client needs to include those parts that are omitted by default, they can explicitly request them by using the expand query parameter. This parameter specifies a comma-separated list of entities that the client wants expanded, identifying each of them by name.

For example, if you are requesting product information and you also need price and availability data, you would use the expand query parameter like this:
```
GET /dw/shop/v23_2/products/123?expand=availability,prices HTTP/1.1
```
This request would return the product information along with the price and availability, which might not be included in a response without the expand parameter


## Hook
https://beeit.io/blog/salesforce-commerce-cloud-ocapi-and-hooks

package.json
```
{
	"hooks": "./cartridge/scripts/hooks.json"
}
```
hooks.json
```
{
    "hooks": [
        {
            "name": "dw.ocapi.shop.customer.address.afterPATCH",
            "script": "./cartridge/scripts/hooks/addressAfterPatch"
        }
    ]
}
```
addressAfterPatch.js
```
exports.afterPATCH = function (customer, addressName, customerAddress) {
	// code
}
```

# Configuration

## Stock 
- Pre-Order/Backorder Handling : A value that specifies a product's availability if it's not in stock. The possible values are NONE, PREORDER, and BACKORDER
- Pre-Order/Backorder Allocation : The quantity of products available for order if the product is out of stock

## Service
Need to create first a profile and a credential
- profile is timeout, circuit breaker, rate limit, etc
- credential is credential
Service can be http, http form (construct a URL encoded form POST), SFTP, etc
- - https://developer.salesforce.com/docs/commerce/b2c-commerce/guide/b2c-webservices.html#web-service-rate-limiter-and-circuit-breaker

Mock callback : In Business Manager, select `Administration > Operations > Services`. For the Service Mode, select Mocked. Then Provide a mockCall callback handler :
```
mockCall: function(svc:HTTPService, client:HTTPClient){
    return {
        statusCode: 200,
        statusMessage: "Success",
        text: "MOCK RESPONSE (" + svc.url + ")"
        };
    }
```

## Tax : 
- Tax classes : creer une classe de tax qu'on peut réutiliser ailleurs (standard, exempt, etc)
- Tax juridiction : créer une zone Canada, US, France, etc
- Tax Rate : tableau de taxe pour chaque juridication croisée avec autant de classes qu'existant (France : standard, exempt)

## Various
Taxation net or gross can only be setup at the site creation.

Les product set ont une entrée à part de product

## Payment processor
Payment method are linked to a payment processor (methods are the display, processor is the mechanism let's say). To define preferences for custom payment processor, define an attribute group named with the same name (like BASIC_CREDIT for the BASIC_CREDIT payment processor) for the SitePreferences system object, and add attributes to it.

## import
Merchant tools
- Content : image, lib
- Customers : customer, product list (wishlist, etc), customer group
- custom object : custom object
- catalog : catalog, pricebook, inventory
- Order : order, tax table, shipping method, payment method
- search : search setting, sorting rules
- Online marketing : plein de trucs genre les coupons

Administration : 
- import et export : meta data (i.e., system type extensions, custom object types, custom preference definitions)
- Site import et export : Tout
- Operation : jobs, services